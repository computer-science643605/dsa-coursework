#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Defined constants for students  details
#define MAX_NAME_LENGTH 50
#define DATE_OF_BIRTH_LENGTH 11
#define REGISTRATION_NUMBER_LENGTH 7
#define PROGRAM_CODE_LENGTH 4
#define MAX_STUDENTS 100

// Defined a structure to represent a student
typedef struct {
    char name[MAX_NAME_LENGTH];
    char dateOfBirth[DATE_OF_BIRTH_LENGTH];
    char registrationNumber[REGISTRATION_NUMBER_LENGTH];
    char programCode[PROGRAM_CODE_LENGTH];
    float annualTuition;
} Student;

// Function to create a new student
void createStudent(Student students[], int* n) {
    printf("Enter student details:\n");
    printf("Name: ");
    scanf(" %[^\n]", students[*n].name);
    printf("Date of Birth (YYYY-MM-DD): ");
    scanf(" %[^\n]", students[*n].dateOfBirth);
    printf("Registration Number (6-digit numeric): ");
    scanf(" %[^\n]", students[*n].registrationNumber);
    printf("Program Code: ");
    scanf(" %[^\n]", students[*n].programCode);
    printf("Annual Tuition: ");
    scanf("%f", &students[*n].annualTuition);

    // Ensure annual tuition is not zero
    while (students[*n].annualTuition <= 0) {
        printf("Annual Tuition must be greater than zero. Please enter again: ");
        scanf("%f", &students[*n].annualTuition);
    }

    (*n)++;
}

// Function to display all student details
void readStudents(Student students[], int n) {
    printf("Student details:\n");
    for (int i = 0; i < n; i++) {// Looping through each student detail
        printf("Student %d:\n", i + 1);
        printf("Name: %s\n", students[i].name);
        printf("Date of Birth: %s\n", students[i].dateOfBirth);
        printf("Registration Number: %s\n", students[i].registrationNumber);
        printf("Program Code: %s\n", students[i].programCode);
        printf("Annual Tuition: %.2f\n", students[i].annualTuition);
    }
}

 // Function to search a student by registration number
void searchStudentByRegistrationNumber(Student students[], int n, char regNumber[]) {
    for (int i = 0; i < n; i++) {
        if (strcmp(students[i].registrationNumber, regNumber) == 0) {
            printf("Student found:\n");
            printf("Name: %s\n", students[i].name);
            printf("Date of Birth: %s\n", students[i].dateOfBirth);
            printf("Registration Number: %s\n", students[i].registrationNumber);
            printf("Program Code: %s\n", students[i].programCode);
            printf("Annual Tuition: %.2f\n", students[i].annualTuition);
            return;
        }
    }
    printf("Student with registration number %s not found.\n", regNumber); // Printed message if student is not found
}

// Function to delete a student record
void deleteStudent(Student students[], int* n, int index) {
    if (index < 0 || index >= *n) {
        printf("Invalid index.\n");
        return;
    }
    for (int i = index; i < *n - 1; i++) {  // Shifting student details to cover the deleted one
        strcpy(students[i].name, students[i + 1].name);
        strcpy(students[i].dateOfBirth, students[i + 1].dateOfBirth);
        strcpy(students[i].registrationNumber, students[i + 1].registrationNumber);
        strcpy(students[i].programCode, students[i + 1].programCode);
        students[i].annualTuition = students[i + 1].annualTuition;
    }
    (*n)--; // Decrementing the number of students
    printf("Student deleted successfully.\n");
}

    // Function to sort students by name
void sortStudentsByName(Student students[], int n) {
    // Bubble sort by name
    for (int i = 0; i < n - 1; i++) {
        for (int j = 0; j < n - i - 1; j++) {
            if (strcmp(students[j].name, students[j + 1].name) > 0) { // Comparing names
            
                Student temp = students[j];
                students[j] = students[j + 1];
                students[j + 1] = temp;
            }
        }
    }
}

void sortStudentsByRegistrationNumber(Student students[], int n) {
    // Bubble sort by registration number
    for (int i = 0; i < n - 1; i++) {
        for (int j = 0; j < n - i - 1; j++) {
            if (strcmp(students[j].registrationNumber, students[j + 1].registrationNumber) > 0) {
                // Swap
                Student temp = students[j];
                students[j] = students[j + 1];
                students[j + 1] = temp;
            }
        }
    }
}

void exportToCSV(Student students[], int n, char* filename) {
    FILE* fp = fopen(filename, "w");
    if (fp == NULL) {
        printf("Error opening file for writing.\n");
        return;
    }

    for (int i = 0; i < n; i++) {
        fprintf(fp, "%s,%s,%s,%s,%.2f\n", students[i].name, students[i].dateOfBirth, students[i].registrationNumber, students[i].programCode, students[i].annualTuition);
    }

    fclose(fp);
    printf("Student records exported to %s successfully.\n", filename);
}

int main() {
    Student students[MAX_STUDENTS]; // Assuming maximum 100 students
    int n = 0; // Number of students

    int choice;
    do {
        printf("\n1. Create student\n");
        printf("2. Read students\n");
        printf("3. Search student by registration number\n");
        printf("4. Delete student\n");
        printf("5. Sort students\n");
        printf("6. Export student records to CSV file\n");
        printf("7. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice) {
        case 1:
            createStudent(students, &n);
            break;
        case 2:
            readStudents(students, n);
            break;
        case 3: {
            char regNumber[REGISTRATION_NUMBER_LENGTH];
            printf("Enter registration number to search: ");
            scanf("%s", regNumber);
            searchStudentByRegistrationNumber(students, n, regNumber);
            break;
        }
        case 4: {
            int index;
            printf("Enter index of student to delete (0 to %d): ", n - 1);
            scanf("%d", &index);
            deleteStudent(students, &n, index);
            break;
        }
        case 5: {
            // Sorting submenu
            int sortChoice;
            printf("Select sorting option:\n");
            printf("1. Sort by name\n");
            printf("2. Sort by registration number\n");
            printf("Enter your choice: ");
            scanf("%d", &sortChoice);

            switch (sortChoice) {
            case 1:
                sortStudentsByName(students, n);
                printf("Students sorted by name.\n");
                break;
            case 2:
                sortStudentsByRegistrationNumber(students, n);
                printf("Students sorted by registration number.\n");
                break;
            default:
                printf("Invalid choice! Please enter 1 or 2.\n");
            }
            break;
        }
        case 6:
            exportToCSV(students, n, "student_records.csv");
            break;
        case 7:
            printf("Exiting program...\n");
            break;
        default:
            printf("Invalid choice! Please enter a number between 1 and 7.\n");
        }
    } while (choice != 7);

    return 0;
}