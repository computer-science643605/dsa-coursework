#Data Structure Selection Considerations

In this project, We have chosen to work with a simple array-based data structure to store information about students enrolled in the School of Computing and Informatics Technology (SCIT). Below are the considerations that influenced the decision:

## Simplicity and Ease of Implementation:

For the purpose of this project, simplicity and ease of implementation are crucial factors. An array-based data structure is straightforward to understand and implement, making it suitable for managing a relatively small collection of student records.

## Sequential Access:

Since the application mainly involves sequential access to student records, an array provides efficient access to elements by index. This makes it convenient for operations such as reading, updating, and deleting student records.


## Direct Access to Elements:

Arrays offer direct access to elements based on their index, which facilitates efficient searching and sorting operations. This is advantageous for functionalities such as searching for students by registration number and sorting students based on various criteria.

By considering these factors, we have determined that an array-based data structure is well-suited for managing student records in this project. 

#NAKABAALE BENJAMIN
Part(a)   https://youtu.be/AXrdjpwMbZY?si=oJ8JLk9vPs1ozW0u

#SYLVIA
this is the youtube link to the explanation for part b) https://youtu.be/BQfr3iChiPw

#KIWANUKA ISAAC lUSUKU
Part(c)   youtube link => https://youtu.be/TLv7CceyLy8